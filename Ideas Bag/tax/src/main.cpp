/*
 * Petross404 (petross404@gmail.com)
 * 
 * Calculate the total cost based on the tax rate
 */

#include <tax_facilities.h>

int main (int argc, char** argv)
{
	std::string str_cost, str_tax_rate;
#if __linux__
    float cost{0}, tax_rate{0}, tot_cost{0};
#elif __MACH__
    float cost=0, tax_rate=0, tot_cost=0;
#endif
    std::cout << "Enter cost : ";
	std::cin >> str_cost;
	
	/*check if there is a dollar or euro sign in there
	answer*/
	if (str_cost.find("$") != std::string::npos)
	{
		std::cout << "You live in USA\n";
	}
	else if (str_cost.find("€") != std::string::npos)
	{
		std::cout << "You live in E.E.\n";
	}
	//Convert string to float
	cost = stof(str_cost.c_str());

	std::cout << "Enter tax rate : ";
	std::cin >> str_tax_rate;
	
	if(str_tax_rate.find("‰") != std::string::npos)
	{
		tax_rate = stof(str_tax_rate.c_str()) / 1000;
	}
	else
	{
		//We can assume that the tax rate is %
		tax_rate = stof(str_tax_rate.c_str()) / 100;
	}

	tot_cost = cost + (cost * tax_rate );
	std::cout << "Total cost : " << tot_cost << std::endl;

	return (0);
}
